"""
Minutes to hours
    changes minutes into hours:minutes format
"""

from sys import stdin

im = int(stdin.read())

if im >= 60:
    h = im // 60
    m = im % 60
    print(f"{h}:{m}")
else:
    print(str(im))
