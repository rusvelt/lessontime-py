from time import localtime
from sys import argv


# cli args
v = p = m = h = False
v = "-v" in argv or "--verbose" in argv
p = "-p" in argv or "--percentage" in argv
m = "-m" in argv or "--minutes" in argv or not p
h = "-h" in argv or "--help" in argv


# Current time
ctime = localtime()
now = ctime.tm_hour*60 + ctime.tm_min
today = ctime.tm_wday


table = [
    [8*60 + 30, 9*60 + 15],
    [9*60 + 25, 10*60 + 10],
    [10*60 + 30, 11*60 + 15],
    [11*60 + 35, 12*60 + 20],
    [12*60 + 30, 13*60 + 15],
    [13*60 + 25, 14*60 + 10],
    [14*60 + 20, 15*60 + 5]
]

# add eight lesson when it needed
l8days = [1]

if today in l8days:
    table.append( [15*60 + 15, 16*60 + 0] )

BEGIN = table[0][0]
END = table[-1][1]


# if before lesson
if now < BEGIN:
    if v:
        print("before")
    print(str(BEGIN- now))

# if after lessons
elif now > END:
    if v:
        print("after")
    print( (24*60 - now) + BEGIN)

# if lessons time
else:
    for i in range(len(table)):

        #if lesson
        if now >= table[i][0] and now <= table[i][1]:
            if v:
                print("lesson")
            print(str(table[i][1] - now))

        # if break
        elif now >= table[i][1] and now <= table[i+1][0]:
            if v:
                print("break")
            print(table[i+1][0] - now)

